graph = {
	1:[2],
	2:[3,4],
	3:[],
	4:[5,6],
	5:[],
	6:[],
}

paths = {}

for i in range(1,7):
	paths[i] = -1

def findAll(graph, node):
	count = 1
	for n in graph[node]:
		if paths[n] == -1:
			paths[n] = findAll(graph, n)
		count += paths[n]
	return count

print findAll(graph, 1)
