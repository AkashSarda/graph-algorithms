
def spanning_tree(graph):
    total_weight = 0
    nodes = []
    nodes.extend(graph.keys())
    edges = []
    max_weight = 0
    _n = nodes[0]
    nodes = nodes[1:]
    edges.extend(graph[_n])
    edges.sort()

    while nodes != []:
        edges.sort()
        min_edge = edges[0]
        edges = edges[1:]

        if min_edge[1] not in nodes:
            continue
        else:
            edges.extend(graph[min_edge[1]])
            nodes.remove(min_edge[1])
            total_weight += min_edge[0]
            if max_weight < min_edge[0]:
                max_weight = min_edge[0]
    return max_weight, total_weight


def prim(graph, start):
        queue = []
        edge_queue = []
        visited = [start]
        node = start
        tree = []

        while True:
            for e in graph[node]:
                if e[1] not in visited:
                    queue.append(e)

            if queue == []:
                break

            queue.sort()
            for edge in queue:
                if edge[1] not in visited:
                    node = edge[1]
                    tree.append(edge)
                    visited.append(node)
                    queue.remove(edge)
                    break
                queue.remove(edge)


        print tree
# wrong implementation.
def kruskal(graph):
    edge_queue = []
    tree = []
    for node in graph.keys():
        for nei in graph[node]:
            edge_queue.append((nei[0], nei[1], node))

    edge_queue.sort()
    print edge_queue
    visited = []
    while edge_queue != []:
        for e in edge_queue:
            if e[1] not in visited or e[2] not in visited:
                tree.append(e)
                if e[1] not in visited:
                    visited.append(e[1])
                if e[2] not in visited:
                    visited.append(e[2])
                edge_queue.remove(e)
            else:
                edge_queue.remove(e)
    print tree

graph = {}
graph = {
    1: [(1,2), (3,3) , (7,4)],
    2: [(2,3), (3,4)],
    3: [(3,1), (3,4)],
    4: [(7,1), (3,2), (3,3)]
}
print kruskal(graph)
