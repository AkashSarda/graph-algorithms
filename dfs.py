def dfs(node, graph, connected):
    count = 0
    print node
    for n in graph[node]:
        if n not in connected:
            connected.append(n)
            count += 1
            count += dfs(n, graph, connected)
    return count

graph = {}
graph[1] = [2,3,7]
graph[2] = [1,3]
graph[3] = [1,2]
graph[7] = [1]
graph[5] = [6]
graph[6] = [5,8]
graph[8] = [6]

connected = []
libraries = 0
count = 0
for _n in graph.keys():
    if _n not in connected:
        connected.append(_n)
        count += dfs(_n, graph, connected) + 1
        libraries += 1
print libraries, count
