# https://www.hackerrank.com/challenges/bfsshortreach


def find_ans(graph, start):
    queue = []
    nodes = []
    nodes.extend(graph.keys())
    queue.append((nodes[0], nodes[0]))
    nodes = nodes[1:]

    while queue != []:
            node = queue[0]
            queue = queue[1:]
            if node[0] == start:
                min_dist[node[0]] = 0
            else:
                if node[0] in nodes:
                    min_dist[node[0]] = min_dist[node[1]] + 6
                    nodes.remove(node[0])

            for _n in graph[node[0]]:
                if _n
                    queue.append((_n, node[0]))
    print min_dist

graph = {}

graph = {
    1 : [5, 2, 4],
    2 : [1, 5, 4, 3],
    3 : [2, 4],
    4 : [5, 1, 2, 3],
    5 : [1, 2, 4],
}

min_dist = {}
find_ans(graph, 1)
