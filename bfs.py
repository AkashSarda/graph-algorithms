# https://www.hackerrank.com/challenges/bfsshortreach
def bfs(graph, start):
    queue = [start]
    visited = [start]
    node = start
    tree = [start]
    while queue != []:
        node = queue[0]
        queue = queue[1:]
        tree.append(node)

        for n in graph[node]:
            if n not in visited:
                queue.append(n)
                visited.append(n)
    print tree
graph = {}

graph = {
    1 : [5, 2, 4],
    2 : [1, 5, 4, 3],
    3 : [2, 4],
    4 : [5, 1, 2, 3],
    5 : [1, 2, 4],
}

min_dist = {}
bfs(graph, 1)
