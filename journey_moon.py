# hackkerank : https://www.hackerrank.com/challenges/journey-to-the-moon

def f(graph, n):
    countries = []
    for key in graph.keys():
        countries.append(len(graph[key]) + 1)

    _sum = 0
    for _s in countries:
        _sum += _s
    countries.extend([1] * (n - _sum))
    print countries
    pairs = 0
    _sum = countries[0]
    i = 0
    while i < len(countries) - 1:
            pairs += _sum * countries[i + 1]
            _sum += countries[i+1]
            i += 1

    return pairs

n = int(input())
p = int(input())

graph = {}
for _ in range(p):
    t = int(input())
    x = int(input())

    if t in graph.keys():
        graph[t].append(x)
    else:
        if x in graph.keys():
            graph[x].append(t)
        else:
            graph[t] = [x]
print graph
print f(graph, n)
